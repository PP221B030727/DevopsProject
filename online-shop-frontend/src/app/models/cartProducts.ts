import {Product} from "./products";

export interface CartProducts {
  id: number;
  product: Product;

}
