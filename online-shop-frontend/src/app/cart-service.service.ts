import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {CartProducts} from "./models/cartProducts";
import {Product} from "./models/products";

@Injectable({
  providedIn: 'root'
})
export class CartServiceService {

  private readonly API_URL = "http://localhost:8080";
  constructor(private http: HttpClient) { }

  getCartList(): Observable<Product[]> {
    return this.http.get<Product[]>(`${this.API_URL}/basket/all`);
  }

  removeFromCart(id: number): Observable<any> {
    return this.http.delete(`${this.API_URL}/basket/delete/${id}`)
  }
}
