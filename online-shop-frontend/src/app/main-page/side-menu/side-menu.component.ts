import { Component } from '@angular/core';
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {Product} from "../../models/products";
import {CartServiceService} from "../../cart-service.service";
import {CartProducts} from "../../models/cartProducts";

@Component({
  selector: 'app-side-menu',
  templateUrl: './side-menu.component.html',
  styleUrls: ['./side-menu.component.css']
})
export class SideMenuComponent {

  products: Product[]=[];
  constructor(private cartService: CartServiceService) {
  }
  ngOnInit(): void {
    this.getCartProducts()
  }

  public getCartProducts(): void {
    this.cartService.getCartList().subscribe(
      (response: Product[]) => {
        this.products=response;
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  public getSum(): number {
    let sum = 0;
    for(const product of this.products) {
      sum += product.price;
    }
    return sum;
  }

  public removeFromCart(id: number) {
    this.cartService.removeFromCart(id).subscribe(() => {
      this.products = this.products.filter((product) => product.id !== id)
    });
  }
}
