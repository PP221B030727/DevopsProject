from datetime import datetime, timedelta
time=datetime.now()
print(f"cron works {time}")
import psycopg2
db_config = {
    "dbname": "postgres",
    "user": "postgres",
    "password": "postgres",
    "host": "postgresql",
    "port": "5432",
}


def etl_job():
  
    try:
        conn = psycopg2.connect(**db_config)
        cursor = conn.cursor()
        create_table_sql = """
        CREATE TABLE IF NOT EXISTS etl_results (
            id SERIAL PRIMARY KEY,
            num_purchases INT,
            total_amount_spent NUMERIC(10, 2),
            created_at TIMESTAMP
        );
        """
        # extract
        query = """
          SELECT 
            COUNT(*) AS num_purchases,
            SUM(p.price) AS total_amount_spent
          FROM basket as b
          JOIN product p ON b.product_id = p.product_id;
        """
        cursor.execute(create_table_sql)
        cursor.execute(query  )
        result = cursor.fetchone()
        print(result)
    



        if result: # transfrom load
            print("if result")
            insert_result_sql = """
            INSERT INTO etl_results (num_purchases, total_amount_spent, created_at)
            VALUES (%s, %s, %s);
            """
            print(f"Last minute: {result}")
            current_time = datetime.now()  
            cursor.execute(insert_result_sql, (result[0], result[1], current_time))

        conn.commit()
        conn.close()

    except (Exception, psycopg2.Error) as error:
        print("Error connecting to PostgreSQL:", error)

etl_job()